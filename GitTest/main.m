//
//  main.m
//  GitTest
//
//  Created by Hikmat Habibullaev on 6/20/18.
//  Copyright © 2018 Hikmat Habibullaev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
