//
//  AppDelegate.h
//  GitTest
//
//  Created by Hikmat Habibullaev on 6/20/18.
//  Copyright © 2018 Hikmat Habibullaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

